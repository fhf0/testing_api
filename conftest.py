import pytest
import requests
import allure


# При инициализации объекта класс принимает в себя аргумент base_address,
# на его место нужно записать в фикстуре общий адрес для всех запросов.
class ApiClient:
    def __init__(self, base_address):
        self.base_address = base_address

    # Наш клиент является оберткой над библиотекой requests.
    # В классе ApiClient аргументы path, params, headers метода get выступают в роли аргументов,
    # которые передаются в requests.get(url=url, params=params, headers=headers).
    # Из адреса, использованного при инициализации объекта и пути, который мы передадим
    # в дальнейшем в ходе теста, будет складываться полный адрес, который и будет использоваться в requests.get

    def get(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'GET request to: {url}'):
            return requests.get(url=url, params=params, headers=headers)

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'POST request to: {url}'):
            return requests.post(url=url, params=params, data=data, json=json, headers=headers)


@pytest.fixture
def dog_api():
    return ApiClient(base_address="https://dog.ceo/api/")
# Фикстура возвращает объект с базовым адресом для дальнейших GET и POST запросов
